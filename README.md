# Requirements

Python 3.9+
Ghostscript (Link)[https://wiki.scribus.net/canvas/Installation_and_Configuration_of_Ghostscript]
Pillow (Link)[https://pillow.readthedocs.io/en/stable/installation.html]

Launch `python3 -m venv venv`
Execute `source venv/bin/activate`

Once inside the console run `python harmonograph.py`
