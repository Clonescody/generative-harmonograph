#!/usr/bin/python3
#   HarmonographTurtle.py
#   An extensible harmonograph simulator - easily change # of pendulums
#   or number of dimensions to plot in. Usually harmonographs are drawn
#   using 2 pendulums in 2 dimensions, but you can add more!
#   Each run plots a random harmonograph, choosing a near-integer number
#   (a fuzzy integer) for each frequency. This tends to generate good pics.
#   Click on the canvas to exit.
#   Authour: Alan Richmond, Python3.codes
#   nit = not in trinket

from turtle import *
import random as r
import colorsys                # nit
from math import sin, pi
from PIL import Image 
from datetime import datetime
import os



def generate_canvas():
    colour = True                  # colour on black, or black on white?
    if colour:  bgc = "white"
    else:       bgc = "white"
    width, height = 400, 400        # canvas (window) size
    npen = 4                        # number of pendulums
    dd = 1.000                     # decay factor
    dt = 0.020                      # time increment
    sd = 0.015                      # frequency spread (from integer)
    mf = 5                          # max range for frequencies
    end = 0.25                     # end when decay this low
    linewidth = 0.88                   # line width
    hui = dt/(2*pi)                 # hue increment (for colour)
    xscale, yscale = width / (npen * 1.8), height / (npen * 1.8)
    
    def fuzzint(mf, sd):
    #    return r.gauss(r.randint(1, mf), sd) # nit gauss()
        return r.randint(1, mf) + sd  # so just add some small number
    
    #   Compute frequencies & phases
    fxy = [[fuzzint(mf, sd)     for p in range(npen)] for q in range(2)]
    #pxy = [[r.uniform(0, 2*pi)  for p in range(npen)] for q in range(2)]  # nit uniform()
    pxy = [[r.randint(0, 7)  for p in range(npen)] for q in range(2)]
    
    canvas = Screen()               # Set up Turtle canvas (or window)
    canvas.setup(width, height)
    canvas.bgcolor(bgc)
    #canvas.title("Harmonograph")   # nit
    pen = Turtle()
    pen.width(linewidth)
    pen.speed("fastest")
    # def exit(x, y): canvas.bye()    # exit if canvas is clicked
    # canvas.onclick(exit)

    hue = 0
    t = 0.0                         # time or theta, take your pick
    dec = 1.0                       # decay factor
    first = True                    # to avoid line from origin
    pen.penup()
    
    def stop(x,y): 
        pen.penup()
        pen.hideturtle()
        print ("saving ....")
        date = (datetime.now()).strftime("%d%b%Y-%H%M%S") 
        fileName = 'posta-' + date
        canvas.getcanvas().postscript(file= fileName+'.eps')
        img = Image.open(fileName + '.eps') 
        img.save(fileName + '.png')
        os.remove(fileName + '.eps')

    canvas.onclick(stop) # if canvas is clicked

    while first or dec > end and pen.isdown():               # stop when close to center or on stop
                                        # calculate next x,y point along line
        xy = [sum(sin(t * fxy[q][p] + pxy[q][p])    for p in range(npen))
                                                    for q in range(2)]
        if colour:  pen.color(colorsys.hsv_to_rgb(hue, 1.0, 1.0))
        pen.goto(xy[0]*xscale*dec, xy[1]*yscale*dec)
        dec *= dd
        hue += hui
        if first:                   # if we just came from origin
            first = False
            pen.pendown()               # put the pen down
        t += dt                     # increment angle for sine
        

generate_canvas()